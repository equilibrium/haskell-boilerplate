#!/usr/bin/env sh

printf "%s\n%s\n" \
    "$(find src test -type f -name \*.hs)" \
    "$(find . -type f -name \*.cabal)" \
    | entr -c cabal test

