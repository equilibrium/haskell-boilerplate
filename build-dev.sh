#!/usr/bin/env sh

printf "%s\n%s\n" \
    "$(find src -type f -name \*.hs)" \
    "$(find . -type f -name \*.cabal)" \
    | entr -c "./.build-dev.sh"
